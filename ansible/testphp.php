<?php
print ("<table border=\"1\">
<thead>
    <tr>
	<th>Параметр</th>
	<th>Значение</th>
    </tr>
</thead>

<tbody>

    <tr>
	<td>Сейчас:</td>
	<td>" . date('H:i:s d.m.Y') . "</td>
    </tr>

    <tr>
	<td>Ваш браузер: </td>
	<td>" . $_SERVER['HTTP_USER_AGENT'] . "</td>
    </tr>

    <tr>
	<td>Ваш IP:</td>
	<td>" . getenv('REMOTE_ADDR') . "</td>
    </tr>

</tbody>

</table>");
?>